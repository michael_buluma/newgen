var fields = ['rectifiers_status', 'aircon_status'],
    job_type = '';
    
for (var i in fields) {
    if (ctx.instance.isChanged(fields[i]) && ctx.instance.getValueByField(fields[i]) == 'NoK') {
        if (fields[i] == 'rectifiers_status') {
            job_type = 'Rectifier';
            createTT(job_type);
        } else if (fields[i] == 'aircon_status') {
            job_type = 'Aircon';
            createTT(job_type);
        }
    }
}

function createTT(job_type) {
    var ttModel = custom.getModelByAlias('project_fault_tickets');
    var site = custom.getModelByAlias('sites').find({ id: ctx.instance.getValueByField('sys_location') })[0];
    
    if(!utils.isEmpty(site)){
        var rec = ttModel.create({
            battery_2_ac_dc_and_site_survey_id: ctx.instance.getValueByField('id'),
            project_id: ctx.instance.getValueByField('project_id'),
            ticket_job_type: job_type + ' Faulty at site ' + site.getValueByField('atoll_id') + ' - ' + site.getValueByField('site_name'),
            ticket_description: job_type + ' Faulty at site ' + site.getValueByField('atoll_id') + ' - ' + site.getValueByField('site_name'),
            issue_logged_at: ctx.instance.getValueByField('updated_at'),
            reported_by: nascUtils.getCustomUserIdBySystemUserId(ctx.instance.getValueByField('created_by')),
            status: "assigned",
            priority: '2',
            contractor: nascUtils.getCompanyIdBySystemUserId(ctx.currentUser.getId()),
            sys_location: ctx.instance.getValueByField('sys_location'),
            assignee: ctx.instance.getValueByField('assignee'),
            sys_position_lat_site: site.getValueByField('sys_position_lat'),
            sys_position_long_site: site.getValueByField('sys_position_long')
        });

        if (!utils.isEmpty(rec.getErrors())) {
            p.log.error('Error: ' + rec.getErrors());
        }
    }
}