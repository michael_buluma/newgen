var fields = ['status'],
    job_type = '';
    
for (var i in fields) {
    if (ctx.instance.isChanged(fields[i]) && ctx.instance.getValueByField(fields[i]) == 'completed') {
        if (fields[i] == 'status') {
            job_type = 'CM';
            createTT(job_type);
        }
    }
}

function createTT(job_type) {
    var ttModel = custom.getModelByAlias('break_management');
    var site = custom.getModelByAlias('sites').find({ id: ctx.instance.getValueByField('sys_location') })[0];
    
    
    if(!utils.isEmpty(site)){
        var rec = ttModel.create({
            incident_number: ctx.instance.getValueByField('inc_number'),
            fault_description: ctx.instance.getValueByField('liquid_fault_details'),
            fault_start_date_time: ctx.instance.getValueByField('date_time_fault_located'),
            region: ctx.instance.getValueByField('region'),
            status: "completed",
            sys_location: ctx.instance.getValueByField('sys_location'),
            technicians: ctx.instance.getValueByField('technician_name'),
            cable_name_fibre_size : cable_tested + ' </br> Fibre Pairs: ' + ctx.instance.getValueByField('fibre_pairs'),
            fault_distance: ctx.instance.getValueByField('distance_from_the_pop'),
            fault_location: ctx.instance.getValueByField('fault_location_address'),
            date_time_cleared: ctx.instance.getValueByField('date_time_cleared'),
            noc_dispatch_time: ctx.instance.getValueByField('date_time_dispatched'),
            noc_dispatch_delay: ctx.instance.getValueByField('delay_time_dispatch'),
            access_delays_noc_info_delays_deferrals: ctx.instance.getValueByField('delay_time_client_access'),
            rfo_delay_comment: ctx.instance.getValueByField('how_was_the_fault_caused'),
            type_of_repairs: ctx.instance.getValueByField('repair_status'),
            fault_cause: ctx.instance.getValueByField('fault_cause'),
            clearance_action: ctx.instance.getValueByField('clearance_action'),
            sys_position_lat_site: site.getValueByField('sys_position_lat'),
            sys_position_long_site: site.getValueByField('sys_position_long')
        });

        if (!utils.isEmpty(rec.getErrors())) {
            p.log.error('Error: ' + rec.getErrors());
        }
    }
}